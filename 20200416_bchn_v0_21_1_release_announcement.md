April 16, 2020

# Release announcement: Bitcoin Cash Node v0.21.1

The Bitcoin Cash Node (BCHN) project is pleased to announce its minor release
version 0.21.1.

This release, which is optional to install before the network upgrade on
15 May 2020, contains a number of bugfixes and performance improvements.

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v0.21.1

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

Lastly, the BCHN crowdfunding campaign is launching tomorrow on Flipstarter:

  https://bchn.flipstarter.cash

We thank everyone for their support and especially the Flipstarter team for
their assistance in translating our proposal to Chinese and bringing
assurance contract funding technology to the world of Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.
